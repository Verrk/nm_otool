/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   nm.h                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/04 02:30:11 by cpestour          #+#    #+#             */
/*   Updated: 2016/01/05 12:30:53 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef NM_H
# define NM_H

# include <libft.h>
# include <printf.h>
# include <mach-o/loader.h>
# include <mach-o/nlist.h>
# include <fcntl.h>
# include <sys/stat.h>
# include <sys/mman.h>
# include <ar.h>

typedef struct mach_header_64		t_header;
typedef struct load_command		t_ld;
typedef struct segment_command_64	t_sg;
typedef struct section_64		t_sc;

typedef struct				s_otool
{
  char	*prog;
  char	*filename;
  char	*ofile;
  char	member[255];
  void	*file;
  int	split;
}					t_otool;

void		*map_file(char *filename, char *prog, int fd, size_t size);
size_t		get_size(char *filename, char *prog, int fd);
int			get_fd(char *filename, char *prog);
void		print_name(char *name, char *obj);
int			get_s_name(void *file);

void		ft_otool(t_otool *o);

#endif
