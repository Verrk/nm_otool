#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/12/22 15:21:36 by cpestour          #+#    #+#              #
#    Updated: 2016/01/05 13:02:37 by cpestour         ###   ########.fr        #
#                                                                              #
#******************************************************************************#

CC=gcc
CFLAGS=-Wall -Werror -Wextra -Iincludes -Ilibft/includes
LDFLAGS=-Llibft -lft
O_DIR=srcs/otool/
N_DIR=srcs/nm/
O_FILES=main.c otool.c
N_FILES=main.c
O_SRC=$(addprefix $(O_DIR), $(O_FILES))
N_SRC=$(addprefix $(N_DIR), $(N_FILES))
O_OBJ=$(O_SRC:.c=.o)
N_OBJ=$(N_SRC:.c=.o)

all: lib ft_otool #ft_nm

lib:
	make -C libft

ft_otool: $(O_OBJ) srcs/map.o
	$(CC) -o $@ $^ $(LDFLAGS)

ft_nm: $(N_OBJ)
	$(CC) -o $@ $^ $(LDFLAGS)

srcs/map.o: srcs/map.c
	$(CC) -o $@ -c $^ $(CFLAGS)

%.o: %.c
	$(CC) -o $@ -c $< $(CFLAGS)

clean:
	make clean -C libft
	rm -f $(O_OBJ) $(N_OBJ) srcs/map.o
	rm -f *~ includes/*~ srcs/*~ srcs/otool/*~ srcs/nm/*~

fclean: clean
	make fclean -C libft
	rm -f ft_otool ft_nm

re: fclean all