/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/21 15:01:23 by cpestour          #+#    #+#             */
/*   Updated: 2015/12/18 17:51:24 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int			ft_putnbr_u(unsigned long long n)
{
	int		i;

	i = 0;
	if (n >= 10)
	{
		i += ft_putnbr_u(n / 10);
		i += ft_putnbr_u(n % 10);
	}
	else
		i += ft_putchar(n + '0');
	return (i);
}

int			ft_putnbr(long long n)
{
	int		i;

	i = 0;
	if (n >= 10 || n <= -10)
	{
		i += ft_putnbr(ft_abs(n / 10));
		i += ft_putnbr(ft_abs(n % 10));
	}
	else
		i += ft_putchar(ft_abs(n) + '0');
	return (i);
}
