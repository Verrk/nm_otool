/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putwstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/29 14:47:55 by cpestour          #+#    #+#             */
/*   Updated: 2015/05/01 14:19:28 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

int			ft_putwstr(wchar_t *str, int prec)
{
	int		i;
	int		ret;
	int		c;

	i = 0;
	ret = 0;
	if (prec < 0)
		prec = 4096;
	while (str[i] && prec > 0)
	{
		c = ft_putwchar(str[i], prec);
		prec -= c;
		ret += c;
		i++;
	}
	return (ret);
}
