/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   flags.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: verrk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/15 13:31:29 by cpestour          #+#    #+#             */
/*   Updated: 2015/01/21 19:05:29 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "printf.h"

static int	get_opt(const char *format, t_printf *p)
{
	int		i;

	i = -1;
	while (format[++i])
	{
		if (format[i] == '#')
			p->flags.sharp = 1;
		else if (format[i] == '0')
			p->flags.zero = p->flags.minus ? 0 : 1;
		else if (format[i] == '-')
		{
			p->flags.minus = 1;
			p->flags.zero = 0;
		}
		else if (format[i] == ' ')
			p->flags.space = p->flags.plus ? 0 : 1;
		else if (format[i] == '+')
		{
			p->flags.plus = 1;
			p->flags.space = 0;
		}
		else
			return (i);
	}
	return (i);
}

static int	get_width(const char *format, t_printf *p)
{
	p->flags.width = ft_atoi(format);
	if (p->flags.width)
		return (ft_nbrlen(p->flags.width));
	else
		return (0);
}

static int	get_prec(const char *format, t_printf *p)
{
	if (*format == '.')
	{
		format++;
		p->flags.prec = ft_atoi(format);
		if (p->flags.prec)
			return (ft_nbrlen(p->flags.prec) + 1);
		else
		{
			if (*format == '0')
				return (2);
			else
				return (1);
		}
	}
	return (0);
}

static int	get_mod(const char *format, t_printf *p)
{
	if (ft_strncmp(format, "hh", 2) == 0)
		p->flags.mod = 'H';
	else if (ft_strncmp(format, "h", 1) == 0)
		p->flags.mod = 'h';
	else if (ft_strncmp(format, "ll", 2) == 0)
		p->flags.mod = 'L';
	else if (ft_strncmp(format, "l", 1) == 0)
		p->flags.mod = 'l';
	else if (ft_strncmp(format, "j", 1) == 0)
		p->flags.mod = 'j';
	else if (ft_strncmp(format, "z", 1) == 0)
		p->flags.mod = 'z';
	else
		p->flags.mod = '\0';
	if (ft_isupper(p->flags.mod))
		return (2);
	else if (ft_islower(p->flags.mod))
		return (1);
	else
		return (0);
}

int			get_flags(const char *format, t_printf *p)
{
	int		i;

	i = 0;
	i += get_opt(format, p);
	format += i;
	i += get_width(format, p);
	if (p->flags.width)
		format += ft_nbrlen(p->flags.width);
	i += get_prec(format, p);
	if (p->flags.prec > 0)
		format += ft_nbrlen(p->flags.prec) + 1;
	else
	{
		if (*format == '.')
			format++;
		if (*format == '0')
			format++;
	}
	i += get_mod(format, p);
	return (i);
}
