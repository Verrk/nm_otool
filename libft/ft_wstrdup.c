/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_wstrdup.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/29 16:08:09 by cpestour          #+#    #+#             */
/*   Updated: 2015/04/29 16:10:56 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

wchar_t			*ft_wstrdup(wchar_t *str)
{
	size_t		len;
	wchar_t		*new;
	int			i;

	i = 0;
	if (str == NULL)
		return (NULL);
	len = ft_wstrlen(str) + 1;
	new = (wchar_t *)malloc(sizeof(wchar_t) * len);
	if (new == NULL)
		return (NULL);
	while (str[i])
	{
		new[i] = str[i];
		i++;
	}
	new[i] = '\0';
	return (new);
}
