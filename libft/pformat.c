/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pformat.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/19 17:38:24 by cpestour          #+#    #+#             */
/*   Updated: 2015/04/28 15:05:20 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "printf.h"

static void		print_sign(t_printf *p)
{
	if (p->flags.plus)
		p->ret += ft_putchar('+');
	else if (p->flags.space)
		p->ret += ft_putchar(' ');
	p->ret += ft_putstr("0x");
}

static void		print_space(t_printf *p, int l, int start)
{
	if (start)
	{
		if (p->flags.zero && !p->flags.minus && p->flags.prec == -1)
		{
			print_sign(p);
			p->ret += ft_nputchar('0', p->flags.width - l);
		}
		else if (!p->flags.minus)
		{
			p->ret += ft_nputchar(' ', p->flags.width - l);
			print_sign(p);
		}
		else
			print_sign(p);
	}
	if (p->flags.minus && !start)
		p->ret += ft_nputchar(' ', p->flags.width - l);
}

void			p_format(t_printf *p)
{
	t_ul		ptr;
	int			l;
	char		*str;

	if ((ptr = va_arg(p->va, t_ul)) != 0)
	{
		str = ft_ltoa(ptr, 16);
		l = (p->flags.plus || p->flags.space) ?
			ft_strlen(str) + 3 : ft_strlen(str) + 2;
		l += (int)(p->flags.prec - ft_strlen(str)) > 0 ?
			p->flags.prec - ft_strlen(str) : 0;
		print_space(p, l, 1);
		p->ret += ft_nputchar('0', p->flags.prec - ft_strlen(str));
		p->ret += ft_putstr(str);
		print_space(p, l, 0);
		free(str);
	}
	else
	{
		if (!p->flags.minus)
			p->ret += ft_nputchar(' ', p->flags.width - 3);
		p->ret += ft_putstr("0x0");
		if (p->flags.minus)
			p->ret += ft_nputchar(' ', p->flags.width - 3);
	}
}
