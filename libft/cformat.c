/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cformat.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: verrk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/15 14:15:14 by verrk             #+#    #+#             */
/*   Updated: 2015/05/01 15:53:44 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "printf.h"

void				c_maj_format(t_printf *p)
{
	wchar_t			c;
	unsigned int	size;

	c = (wchar_t)va_arg(p->va, wint_t);
	size = size_bin(c);
	if (size <= 7)
		(p->flags.width)--;
	else if (size <= 11)
		(p->flags.width) -= 2;
	else if (size <= 16)
		(p->flags.width) -= 3;
	else
		(p->flags.width) -= 4;
	if (!p->flags.minus)
	{
		if (p->flags.zero)
			p->ret += ft_nputchar('0', p->flags.width);
		else
			p->ret += ft_nputchar(' ', p->flags.width);
	}
	p->ret += ft_putwchar(c, 4);
	if (p->flags.minus)
		p->ret += ft_nputchar(' ', p->flags.width);
}

void				c_format(t_printf *p)
{
	unsigned char	c;

	if (p->flags.mod == 'l')
	{
		c_maj_format(p);
		return ;
	}
	c = (unsigned char)va_arg(p->va, int);
	(p->flags.width)--;
	if (!p->flags.minus)
	{
		if (p->flags.zero)
			p->ret += ft_nputchar('0', p->flags.width);
		else
			p->ret += ft_nputchar(' ', p->flags.width);
	}
	p->ret += ft_putchar(c);
	if (p->flags.minus)
		p->ret += ft_nputchar(' ', p->flags.width);
}
