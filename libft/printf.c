/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: verrk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/15 10:43:52 by verrk             #+#    #+#             */
/*   Updated: 2015/04/29 16:28:28 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "printf.h"

static void		init_c(t_printf *p)
{
	t_fun		*fun;

	fun = p->fun_tab;
	fun[0].c = 's';
	fun[1].c = 'S';
	fun[2].c = 'p';
	fun[3].c = 'd';
	fun[4].c = 'D';
	fun[5].c = 'i';
	fun[6].c = 'o';
	fun[7].c = 'O';
	fun[8].c = 'u';
	fun[9].c = 'U';
	fun[10].c = 'x';
	fun[11].c = 'X';
	fun[12].c = 'c';
	fun[13].c = 'C';
	fun[14].c = '%';
	fun[15].c = '\0';
}

static void		init_f(t_printf *p)
{
	t_fun		*fun;

	fun = p->fun_tab;
	fun[0].f = &s_format;
	fun[1].f = &s_maj_format;
	fun[2].f = &p_format;
	fun[3].f = &d_format;
	fun[4].f = &d_maj_format;
	fun[5].f = &d_format;
	fun[6].f = &o_format;
	fun[7].f = &o_maj_format;
	fun[8].f = &u_format;
	fun[9].f = &u_maj_format;
	fun[10].f = &x_format;
	fun[11].f = &x_maj_format;
	fun[12].f = &c_format;
	fun[13].f = &c_maj_format;
	fun[14].f = &perc_format;
}

static void		init(t_printf *p)
{
	p->ret = 0;
	init_c(p);
	init_f(p);
}

int				ft_printf(const char *format, ...)
{
	t_printf	*p;
	int			ret;

	p = (t_printf*)malloc(sizeof(t_printf));
	init(p);
	va_start(p->va, format);
	ft_format(format, p);
	va_end(p->va);
	ret = p->ret;
	free(p);
	return (ret);
}
