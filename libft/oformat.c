/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   oformat.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/28 14:33:13 by cpestour          #+#    #+#             */
/*   Updated: 2015/04/29 17:27:34 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "printf.h"

static void		print_sign(t_printf *p, char *str)
{
	if (p->flags.sharp && str[0] != '0')
		p->ret += ft_putchar('0');
}

static void		print_space(t_printf *p, char *str, int start)
{
	int			l;

	l = p->flags.sharp && str[0] != '0' ? 1 : 0;
	l += p->flags.prec > (int)ft_strlen(str) ? p->flags.prec :
		(int)ft_strlen(str);
	if (start)
	{
		if (p->flags.zero && !p->flags.minus && p->flags.prec == -1)
		{
			print_sign(p, str);
			p->ret += ft_nputchar('0', p->flags.width - l);
		}
		else if (!p->flags.minus && p->flags.width > p->flags.prec)
		{
			p->ret += ft_nputchar(' ', p->flags.width - l);
			print_sign(p, str);
		}
		else
			print_sign(p, str);
	}
	if (p->flags.minus && !start)
		p->ret += ft_nputchar(' ', p->flags.width - l);
}

static void		print_o(t_printf *p, t_ull o)
{
	char		*str;

	str = ft_itoa_base(o, 8, 0);
	print_space(p, str, 1);
	p->ret += ft_nputchar('0', p->flags.prec - ft_strlen(str));
	if (o != 0 || (o == 0 && p->flags.prec != 0))
		p->ret += ft_putstr(str);
	else if (o == 0 && p->flags.width)
		p->ret += ft_putchar(' ');
	print_space(p, str, 0);
	free(str);
}

void			o_format(t_printf *p)
{
	t_ull		o;

	if (p->flags.mod == 'h')
		o = (unsigned short)va_arg(p->va, unsigned int);
	else if (p->flags.mod == 'H')
		o = (unsigned char)va_arg(p->va, unsigned int);
	else if (p->flags.mod == 'l')
		o = va_arg(p->va, unsigned long);
	else if (p->flags.mod == 'L')
		o = va_arg(p->va, unsigned long long);
	else if (p->flags.mod == 'j')
		o = va_arg(p->va, uintmax_t);
	else if (p->flags.mod == 'z')
		o = va_arg(p->va, size_t);
	else
		o = va_arg(p->va, unsigned int);
	print_o(p, o);
}

void			o_maj_format(t_printf *p)
{
	t_ull		o;

	o = va_arg(p->va, unsigned long);
	print_o(p, o);
}
