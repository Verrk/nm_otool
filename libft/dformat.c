/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dformat.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/17 02:05:29 by cpestour          #+#    #+#             */
/*   Updated: 2015/12/18 17:50:48 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "printf.h"

static void		print_sign(t_printf *p, long long d)
{
	if (p->flags.plus && d >= 0)
		p->ret += ft_putchar('+');
	else if (p->flags.space && d >= 0)
		p->ret += ft_putchar(' ');
	if (d < 0)
		p->ret += ft_putchar('-');
}

static void		print_space(t_printf *p, long long d, int start)
{
	int			l;

	if (d < 0 || p->flags.plus || p->flags.space)
		l = 1;
	else
		l = 0;
	l += p->flags.prec > (int)ft_nbrlen(d) ? p->flags.prec : (int)ft_nbrlen(d);
	if (start)
	{
		if (p->flags.zero && !p->flags.minus && p->flags.prec == -1)
		{
			print_sign(p, d);
			p->ret += ft_nputchar('0', p->flags.width - l);
		}
		else if (!p->flags.minus && p->flags.width > p->flags.prec)
		{
			p->ret += ft_nputchar(' ', p->flags.width - l);
			print_sign(p, d);
		}
		else
			print_sign(p, d);
	}
	if (p->flags.minus && !start)
		p->ret += ft_nputchar(' ', p->flags.width - l);
}

static void		print_d(t_printf *p, long long d)
{
	print_space(p, d, 1);
	p->ret += ft_nputchar('0', p->flags.prec - ft_nbrlen(d));
	if (d != 0 || (d == 0 && p->flags.prec != 0))
		p->ret += ft_putnbr(d);
	else if (d == 0 && p->flags.width)
		p->ret += ft_putchar(' ');
	print_space(p, d, 0);
}

void			d_format(t_printf *p)
{
	long long	d;

	if (p->flags.mod == 'h')
		d = (short)va_arg(p->va, int);
	else if (p->flags.mod == 'H')
		d = (char)va_arg(p->va, int);
	else if (p->flags.mod == 'l' || p->flags.mod == 'z')
		d = va_arg(p->va, long);
	else if (p->flags.mod == 'L')
		d = va_arg(p->va, long long);
	else if (p->flags.mod == 'j')
		d = va_arg(p->va, intmax_t);
	else
		d = va_arg(p->va, int);
	print_d(p, d);
}

void			d_maj_format(t_printf *p)
{
	long		d;

	d = va_arg(p->va, long);
	print_d(p, d);
}
