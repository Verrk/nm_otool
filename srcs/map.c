/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/05 13:03:12 by cpestour          #+#    #+#             */
/*   Updated: 2016/01/05 13:03:13 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <nm.h>

int			get_s_name(void *file)
{
	return (ft_atoi(file + 3));
}

void		print_name(char *name, char *obj)
{
	ft_printf("%s", name);
	if (obj != NULL)
		ft_printf("(%s)", obj);
	ft_putstr(":\n");
}

int			get_fd(char *filename, char *prog)
{
	int		fd;

	if ((fd = open(filename, O_RDONLY)) < 0)
	{
		ft_printf("%s: open error: %s\n", prog, filename);
		exit(-1);
	}
	return (fd);
}

size_t		get_size(char *filename, char *prog, int fd)
{
	struct stat	st;

	if (fstat(fd, &st) == -1)
	{
		ft_printf("%s: fstat error: %s\n", prog, filename);
		close(fd);
		exit(-1);
	}
	return (st.st_size);
}

void		*map_file(char *filename, char *prog, int fd, size_t size)
{
	void	*ptr;

	ptr = mmap(0, size, PROT_READ | PROT_WRITE, MAP_FILE | MAP_PRIVATE, fd, 0);
	close(fd);
	if (ptr == MAP_FAILED)
	{
		ft_printf("%s: mmap error: %s\n", prog, filename);
		exit(-1);
	}
	return (ptr);
}
