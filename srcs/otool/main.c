/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/05 13:03:51 by cpestour          #+#    #+#             */
/*   Updated: 2016/01/05 13:06:34 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <nm.h>

static void		split_name(t_otool *o, char *filename)
{
  char	*ptr;

  o->filename = filename;
  ptr = ft_strchr(filename, '(');
  if (ptr)
    {
      o->split = 1;
      *ptr = 0;
      o->ofile = ptr + 1;
      while (*ptr != ')')
	ptr++;
      *ptr = 0;
    }
  else
    {
      o->split = 0;
      o->ofile = filename + ft_strlen(filename);
    }
}

int			main(int ac, char **av)
{
	int		i;
	t_otool		o;

	i = 1;
	o.prog = av[0];
	if (ac == 1)
		ft_printf("%s: at least one file must be specified\n", av[0]);
	else
	{
		while (i < ac)
		{
		  split_name(&o, av[i]);
			ft_otool(&o);
			i++;
		}
	}
	return (0);
}
