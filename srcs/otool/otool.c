/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   otool.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/04 03:37:32 by cpestour          #+#    #+#             */
/*   Updated: 2016/01/05 12:58:01 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <nm.h>

static void	print_content(char *str, int size)
{
	int		i;

	i = 0;
	while (i < size)
	{
		ft_printf(" %.2x", 0xFF & str[i]);
		i++;
	}
	ft_putchar('\n');
}

static void	print_sec(void *file, t_sc *sec)
{
	uint64_t	i;
	uint64_t	addr;
	void		*off;

	i = 0;
	addr = sec->addr;
	off = file + sec->offset;
	while (i < sec->size - 16)
	{
		ft_printf("%.16x", addr + i);
		print_content(off + i, 16);
		i += 16;
	}
	if (sec->size % 16 != 0)
	{
		ft_printf("%.16x", addr + i);
		print_content(off + i, sec->size % 16);
	}
}

static void	find_section(void *file, t_sg *seg, uint32_t nsects)
{
	uint32_t	i;
	t_sc		*sec;
	void		*ptr;

	i = 0;
	ptr = (void *)seg + sizeof(t_sg);
	while (i < nsects)
	{
		sec = (t_sc *)ptr;
		if (ft_strequ(sec->sectname, SECT_TEXT))
		{
			ft_printf("(__TEXT,__text) section\n");
			print_sec(file, sec);
		}
		ptr += sizeof(t_sc);
		i++;
	}
}

static void	find_segment(void *file, uint32_t ncmds, uint32_t type)
{
	uint32_t	i;
	t_ld		*lc;
	t_sg		*seg;
	void		*ptr;

	i = 0;
	ptr = file + sizeof(t_header);
	while (i < ncmds)
	{
		lc = (t_ld *)ptr;
		if (lc->cmd == LC_SEGMENT_64)
		{
			seg = (t_sg *)lc;
			if (type == MH_OBJECT)
			    find_section(file, seg, seg->nsects);
			else if (ft_strequ(seg->segname, SEG_TEXT))
			  find_section(file, seg, seg->nsects);
		}
		ptr += lc->cmdsize;
		i++;
	}
}

void		for_ar(t_otool *o, int size)
{
	int		s;
	int		s_name;
	t_header	*head;
	void		*ptr;

	ptr = o->file;
	o->file += SARMAG;
	s = ft_atoi(o->file + 48);
	o->file += sizeof(struct ar_hdr) + s;
	ft_printf("Archive : %s\n", o->filename);
	while (o->file - ptr < size)
	{
		s = ft_atoi(o->file + 48);
		s_name = get_s_name(o->file);
		o->file += sizeof(struct ar_hdr);
		ft_strncpy(o->member, o->file, s_name);
		o->file += s_name;
		head = (t_header *)o->file;
		if (o->split == 0 || ft_strequ(o->member, o->ofile))
		  {
		    print_name(o->filename, o->member);
		    find_segment(o->file, head->ncmds, head->filetype);
		  }
		o->file += s - s_name;
	}
}

void		ft_otool(t_otool *o)
{
	int		fd;
	size_t	size;
	t_header	*head;

	fd = get_fd(o->filename, o->prog);
	size = get_size(o->filename, o->prog, fd);
	o->file = map_file(o->filename, o->prog, fd, size);
	head = (t_header *)o->file;
	if (ft_strncmp(o->file, ARMAG, SARMAG) == 0)
		for_ar(o, size);
	else if (head->magic == MH_MAGIC_64 || head->magic == MH_CIGAM_64)
	  {
	    print_name(o->filename, NULL);
	    find_segment(o->file, head->ncmds, head->filetype);
	  }
	else
		ft_printf("%s: %s is not an object file\n", o->prog, o->filename);
	munmap(o->file, size);
}
